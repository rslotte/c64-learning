/*
https://www.youtube.com/watch?v=bMzBr0BbCBE 2:43:00


.label is a CONST
  No bytes are consumed for the above

screenRam defines what is there

colorRam defines what color it is
	Can't be moved!


0800-9fff

a000-bfff //basic

c000-cfff
d000-dfff // IO - VIC, SID, Color Ram
e000-ffff // Kernal

CPU Flags

NVBDIZC
|	 |
negative flag
is set when we load a negative number?
bmi branch if negative flag is set
bpl branch if negative flag is **not** set
lda #%10000000 will set the negative flag (-128)
	 |
	 zero flag
	 is set when we load something that is zero or inc or dec to 0
	 bne branch if zero flag is **not** set
	 beq branch if zero flag is set
*/

.const SCREEN_RAM = $0400
.const COLOR_RAM = $d800

BasicUpstart2(Entry)

TextMap:
	.import binary "assets/nebulus_map.bin"

ColorRamp:
	.byte $01,$07,$0f,$0a,$0c,$04,$0b,$06,$0b,$04,$0c,$0a,$0f,$07,$01

ColorIndex:
	.byte $00

Entry: {
	// 0001 set the pointer to screen memory to $0400
	//     1000 set the pointer for the char map to $2800
	//bit 76543210    
	lda #%00011000
	sta $d018

	// Set border and background
	lda #0
	sta $d020
	sta $d021

	// jump to subroutine
	// Pushed address of next instruction onto the stack
	// Sets program counter to address of subroutine
	jsr ClearScreen

	// Draw text
	ldx #0
	!:
		lda TextMap, x
		// Start drawing 12 rows down
		// Screen width is 40
		sta SCREEN_RAM + 12 * 40, x
		inx
		cpx #40
		bne !-

	// Set Colors
	Loop:
		ldx ColorIndex
		inx

		// If we're not 14 then branch forwards to the next label
		cpx #14
		bne !+
		// Otherwise set x to 0
		ldx #0
	!:
		// Set the ColorIndex to 0-14
		stx ColorIndex

		// Begin plotting colors in a loop
		ldy #0
	InnerLoop:
		// Load into the A register the address of ColorRamp at offset x
		lda ColorRamp, x
		sta COLOR_RAM + 12 * 40 + 10, y

		// Increment our color offset for the next char
		inx
		cpx #14
		bne !+
		ldx #0
	!:
		// Increase column index
		iny
		cpy #24
		// Have we wrapped around?
		bne InnerLoop

		// Update after raster if under the text
		lda #$9b
	!:
		// Current raster line
		cmp $d012
		bne !-

		jmp Loop
}



ClearScreen: {
	lda $2800
	ldx #250

	!:
		dex
		sta SCREEN_RAM, x
		sta SCREEN_RAM + 250, x
		sta SCREEN_RAM + 500, x
		sta SCREEN_RAM + 750, x
		// Branch to ! if zero flag is not set
		// If the last value loaded, inc or dec is now zero
		// !- is the last blank label (behind) !+ is the next forward
		bne !-
	rts
}

//
// At address $2000
// So the VIC can see it
* = $2000
.import binary "assets/nebulus_chars.bin"