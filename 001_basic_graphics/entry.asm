/*
6510 is an 8-bit chip can see number from 0-255

%11111111 is binary in KickAssembler
255 is decimal
$ff is hexadecimal

* is the current address of where you are

* = $0801 sets us to the beginning of basic

BasicUpstart2 generates the BASIC program that runs our assembler

The CPU has:

* Three number registers
	* Accumulator Register, X Register and Y Register
* A Stack Pointer SP
* Program Counter PC
  * Where the current execution is
* Processor Status Flags
  * Carry etc.  

# Accumulator

* Can store number from 0-255, 0-ff, 00000000-11111111
* lda to store value in accumulator
* Can't inc/dec but can add and subtract
* lda # is immediate mode (store whatever number into the accumulator)
Anything prefixed with # is a literal number value. Any other number refers to a memory location.
# X/X

* Can store values
* Inc or Dec by one

# Commands

OPCODE (lda) OPERAND (zero, one or two) 

inc increase value in memory rather than in a registry

jmp just sets PC without a return address (doesn't push anything on stack)
jsr pushes the next command onto the stack for rts

https://www.youtube.com/watch?v=bMzBr0BbCBE 1:17:23

*/

BasicUpstart2(Entry)

Entry:
	inc Entry + 4
	lda #WHITE
	sta $d020
	sta $d021
	
	// Get the address of the next command and push it to the stack, 
	//then move the program counter to the address of SubRoutine
	jsr SubRoutine 
	lda $ff
	sta $d021

SubRoutine:
	lda #0 // 0-255
	ldx #0 // 0-255
	inx
	inx
	inx
	dex
	stx $d021

	ldy #0 // 0-255
	iny
	dey
	sty $d020

	inc $d021

	// Pops the top value from the stack
	// and sets PC to that value
	rts
