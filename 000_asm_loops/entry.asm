:BasicUpstart2(Entry)

.const BORDER = $d020
.const BACKGROUND = $d021
.const SCREEN = $d0400

Entry:
	lda #WHITE
	sta BACKGROUND
	ldx #$01

Loop:
	stx BORDER
	stx SCREEN
	stx SCREEN + 39
	stx SCREEN + 999 - 39
	stx SCREEN + 999
	inx

	jmp Loop
