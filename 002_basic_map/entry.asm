BasicUpstart2(Entry)

#import "./libs/vic.asm"
#import "./maploader.asm"

/*
https://www.youtube.com/watch?v=z7z4aM6mLmI 2:03:00

*/

Entry:
	lda #BLACK
	sta VIC.BACKGROUND_COLOR
	sta VIC.BORDER_COLOR

	// Bank out BASIC and Kernal ROM
	// Processor ports $0001
	// Load what exists in that location
	lda $01
	// Then and the bits we want to unset
	// Last three bits control visibility of 
	and #%11111000
	ora #%00000101
	sta $01

	// Set VIC Bank 3
	lda $dd00
	and #%11111100
	sta $dd00

	// Set screen and character memory
	lda #%00001100
	sta VIC.MEMORY_SETUP

	// Disable CIA IRQ's
	lda #$7f
	sta $dc0d
	sta $dd0d

	jsr MAPLOADER.DrawMap

	// Inf loop
	jmp *

// In order for these to work they need to be in a location that the VIC can read
#import "assets.asm"