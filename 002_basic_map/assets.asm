/*
C64 has 64k ram

You can use 16k for the VIC

$0400

$0000 - $3fff VIC bank 0 $1000
$4000 - $7fff VIC bank 1
$8000 - $bfff VIC bank 2 $9000 
$c000 - $ffff VIC bank 3

Default char set is at $1000 and $9000
  These are in ROM, VIC will always read the underlying charset
  Therefore try to use eg bank 3 for graphics
  SID can use $1000 for music 

$d000 - $dfff Sprite registers, color ram, sid chip

We will use:

$c000 - $ffff Vic Bank 3

$c000 - $c3ff Screen
$c400 - $c7ff 16 Sprites
$d000 - $efff 128 Sprites
$f000 - $f7ff 1 char
$f800 - $fffd 16 Sprites


Basic sits:
$a000 - $bfff
  This is freed up if we disable basic

*/
.label SCREEN_RAM = $C000
* = $f000 "Charset"
.import binary "./assets/maps/chars.bin"

// This is general data not VIC data so it can go elsewhere
* = $8000 "Map data"

MAP_TILES:
	.import binary "./assets/maps/tiles.bin"

CHAR_COLORS:
	.import binary "./assets/maps/char_cols.bin"

MAP_1:
	.import binary "./assets/maps/map1.bin"


