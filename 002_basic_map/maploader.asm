MAPLOADER: {
	TileScreenLocations:
		// Reference for a tile on the screen
		.byte 0, 1, 40, 41
	Row:
		.byte $00
	Col:
		.byte $00

	DrawMap: {
		// Self modifying code
		// Write the location of screen ram here
		lda #<SCREEN_RAM //$00
		sta Scr + 1
		lda #>SCREEN_RAM //$c0
		sta Scr + 2

		// Self modifying code
		// Write the location of color ram here
		lda #<VIC.COLOR_RAM
		sta Color + 1
		lda #>VIC.COLOR_RAM
		sta Color + 2

		lda #$00
		sta Row

	!RowLoop:
		lda #$00
		sta Col
	!ColumnLoop:
		ldy #$00
	!:
 		lda MAP_TILES + 30 * 4, y // 
 		ldx TileScreenLocations, y
 	Scr:
 		// B00B here indicates we are actually overwriting it
 		// using self-modifying code, why?
		sta $B00B, x // opcode $00 operand $00 $00
		tax
		lda CHAR_COLORS, x
		ldx TileScreenLocations, y
	Color:
		sta $B00B, x // 
		iny
		cpy #$04
		bne !-

		// Now utilise self-modifying code
		// Increment screen ram
		// 16-bit add
		clc
		lda Scr + 1
		adc #$02
		sta Scr + 1
		lda Scr + 2
		adc #$00
		sta Scr + 2

		lda Color + 1
		adc #$02
		sta Color + 1
		lda Color + 2
		adc #$00
		sta Color + 2

		inc Col
		ldx Col
		cpx #20
		bne !ColumnLoop-

		clc
		lda Scr + 1
		adc #$28
		sta Scr + 1
		lda Scr + 2
		adc #$00
		sta Scr + 2

		lda Color + 1
		adc #$28
		sta Color + 1
		lda Color + 2
		adc #$00
		sta Color + 2

		inc Row
		ldx Row
		cpx #11
		bne !RowLoop-

		rts
	}
}

